import {FC, useState, useEffect} from 'react';
import { Input, Button, Text, InputRightElement, InputGroup} from '@chakra-ui/react'
import lightwallet from 'eth-lightwallet';
import { saveAs } from 'file-saver';


const Account: FC = () => {

    const [mnemonic, setMnemonic] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [hdPath, setHdPath] = useState<string>("m/0'/0'/0'")
    const [keystore, setKeystore] = useState<lightwallet.keystore>(Object)
    const [address, setAddress] = useState<string>('')
    
    const [show, setShow] = useState(false)
    const handleClick = () => setShow(!show)
    
    const handleCreateVaultClick = () => {
        lightwallet.keystore.createVault({
            password: password, 
            seedPhrase: mnemonic,
            salt:'han@TISalt',
            // hdPathString: "M/44'/60'/0'/0/1"
            // hdPathString: "m/44'/60'/0'/0/2"
            hdPathString: hdPath
          }, (err, keystore) => {

            keystore.keyFromPassword(password, (err, pwDerivedKey) => {
                console.log(pwDerivedKey)
                keystore.generateNewAddress(pwDerivedKey, 1,);
                setAddress(keystore.getAddresses().toString())
            });
            setKeystore(keystore)
          });
    }
    const handleDownload = () =>{
        const blob = new Blob([keystore.serialize()], {type: "text/plain;charset=utf-8"});
        saveAs.saveAs(blob, "keystore.json");
    }

    useEffect(() => {
        console.log('change')
        console.log(keystore)
        console.log(address)
    }, [keystore, address])

    return (
        <>
            <Text>Mnemonic</Text>
            <Input
                value={mnemonic}
                onChange={(event) => {
                    setMnemonic(event.target.value)
                }}
                pr='4.5rem'
                type='text'
                placeholder='Enter mnemonic'
            />
            <Text>Password</Text>
            <InputGroup size='md'>
                <Input
                    value={password}
                    onChange={(event) => {
                        setPassword(event.target.value)
                    }}
                    pr='4.5rem'
                    placeholder='Enter password'
                    type={show ? 'text' : 'password'}
                />
                <InputRightElement width='4.5rem'>
                    <Button h='1.75rem' size='sm' onClick={handleClick}>
                    {show ? 'Hide' : 'Show'}
                    </Button>
                </InputRightElement>
            </InputGroup>
            <Text>hdPath</Text>
            <Input
                value={hdPath}
                onChange={(event) => {
                    setHdPath(event.target.value)
                }}
                pr='4.5rem'
                type='text'
                placeholder='Enter hdPath'
            />
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                colorScheme="teal"
                width="full"
                onClick={handleCreateVaultClick}
            >
                Create vault
            </Button>
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                colorScheme="teal"
                width="full"
                onClick={handleDownload}
            >
                Download vault
            </Button>
            <Text
            w={500}>
                address : {address}
            </Text>
        </>
    )
}

export default Account