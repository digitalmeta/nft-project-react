import { useForm } from 'react-hook-form'
import { FC, useState, ChangeEvent } from "react";
import {
    Text,
    Input,
    Button,
} from '@chakra-ui/react'
import axios, {AxiosRequestConfig, AxiosPromise} from 'axios';
// import {selfMintingContract}  from "../web3ConfigLocalNet";
import {selfMintingContract}  from "../web3Config";

interface MainProps {
    account: string;
}

const Minting: FC<MainProps> = ({account}) => {
    const { register, handleSubmit } = useForm()
    const [nftName, setNftName] = useState('')
    const [nftDescripton, setNftDescripton] = useState('')
    const [metadata, setMetadata] = useState('')
    const selfMint = async (cid:string) => {
        if (!account) return;
        const contentURI = 'ipfs://' + cid
        const response = await selfMintingContract.methods
            .awardItem(account, contentURI)
            .send({
                from: account
            });
        return response
    }

    const onPinnigJson = async (cid: string) => {
        const jsonUrl = process.env.REACT_APP_PINATA_PIN_JSON_URL
        const pinataJWT = 'Bearer ' + process.env.REACT_APP_PINATA_JWT
        const jsonData = JSON.stringify({
            "pinataOptions": {
                "cidVersion": 0
            },
            "pinataMetadata": {
                "name": "metadata.json"
            },
            "pinataContent": {
                "name": nftName,
                "description": nftDescripton,
                "image": 'ipfs://' + cid,
                "httpUrl": 'https://gateway.pinata.cloud/ipfs/' + cid,
                "attr1": "추가속성 1",
                "attr2": "추가속성 2"
            }
        });
        setMetadata(JSON.stringify({
            "name": nftName,
            "description": nftDescripton,
            "image": 'ipfs://' + cid
        }, null, 4))
        const config = {
            method: 'post',
            url: jsonUrl,
            headers: { 
              'Content-Type': 'application/json', 
              'Authorization': pinataJWT
            },
            data : jsonData
        }
        await axios(config).then((resolve)=>{
            console.log(resolve.data.IpfsHash)
            selfMint(resolve.data.IpfsHash)
        }).catch((reason)=>{
            console.log(reason)
        })
    }

    const onPinnigFile = async (data:FormData): Promise<any> => {
        const fileUrl = process.env.REACT_APP_PINATA_PIN_FILE_URL
        const pinataJWT = 'Bearer ' + process.env.REACT_APP_PINATA_JWT
        if (pinataJWT){
            const config:AxiosRequestConfig<any> = {
                method: 'post',
                url: fileUrl,
                headers: { 
                    'Authorization': pinataJWT
                },
                data: data
            }
            const resolve = await axios(config)
                .catch((reason)=>{
                    console.log(reason)
                    throw new Error("파일 업로드 에러");
                })
            return resolve.data.IpfsHash
        } else {
            throw new Error("JWT 에러");
        }
    }

    const onSubmit = async (data: any) => {
        const formData = new FormData()
        formData.append("file", data.file[0])
        await onPinnigFile(formData).then((resolve) => {
            onPinnigJson(resolve)
        }).catch((reason) => {
            console.log(reason)
        })
        
    }

    return (
        <div className="App">
            <form onSubmit={handleSubmit(onSubmit)}>
                <Input mt={4} size="sm" colorScheme="blue" type="file" {...register("file")} />
                <Text mt={4} size="sm">nft 이름</Text><Input  mt={4} size="sm" type="text" value={nftName} onChange={(e: ChangeEvent<HTMLInputElement>)=>{setNftName(e.target.value)}}/>
                <Text mt={4} size="sm">nft 설명</Text><Input  mt={4} size="sm" type="text" value={nftDescripton} onChange={(e: ChangeEvent<HTMLInputElement>)=>{setNftDescripton(e.target.value)}}/>
                <Button mt={4} size="sm" colorScheme="blue" type='submit'> Mint </Button> 
            </form>
            <Text>
                {metadata}
            </Text>
        </div>
    )
}

export default Minting