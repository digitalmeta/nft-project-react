import React, { FC, useState } from "react";
import { Box, Text, Button } from "@chakra-ui/react"
import lightwallet from 'eth-lightwallet';


const Mnemonic: FC = () => {
    const [mnemonic, setMnemonic] = useState<string>('');

    const onClickGenerateMnemonic:() => void = () => {
        // 임의의 12단어 시드로 구성된 문자열을 생성하고 반환
        setMnemonic(lightwallet.keystore.generateRandomSeed())
    }

    return (
        <>
            <Box>
                {mnemonic ? ( <Text>{mnemonic}</Text> ) : ( <Text>니모닉 코드를 생성하세요</Text> )}
            </Box>
            <Button mt={4} size="sm" colorScheme="blue" onClick={onClickGenerateMnemonic}> Generate </Button>
        </>
    )
}

export default Mnemonic