import { FC} from "react"
import {Stack} from "@chakra-ui/react"
import Mnemonic from "../components/Mnemonic";
import Account from "../components/Account";

const Wallet: FC = () => {
    
    return (
        <Stack
            flexDir="column"
            mb="2"
            justifyContent="center"
            alignItems="center"
        >
            <Mnemonic />
            <Account />
        </Stack>
    )
}
export default Wallet