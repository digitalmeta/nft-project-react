## Application 설치 및 실행
``` shell
$ git clone https://gitlab.com/digitalmeta/nft-project-react.git
$ cd nft-project-react
$ npm install
$ npm start
```

## PINATA API Key





## [eth-lightwallet](https://github.com/ConsenSys/eth-lightwallet)
### 니모닉 코드 생성
``` javascript
lightwallet.keystore.generateRandomSeed()   // 랜덤 니모닉 코드 생성
lightwallet.keystore.createVault({          // 키스토어 생성
    password: string;                       
    seedPhrase: string;                     // 니모닉
    salt?: string | undefined;              // salt(option)
    hdPathString: string;                   // hdpath
}, callback)
```
![니모닉코드 생성 로직](https://steemitimages.com/1280x0/https://github.com/ethereumbook/ethereumbook/raw/develop/images/bip39-part1.png)

## 이더리움 지갑 정리
https://steemit.com/kr-dev/@modolee/mastering-ethereum-4-wallet